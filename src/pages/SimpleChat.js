import React, { useState, useEffect } from "react";
import { Button, Card, Col, Container, Form, FormControl, Image, InputGroup, ListGroup, Row } from "react-bootstrap";
import Swal from "sweetalert2";
import '../styles/base.css'
import { v4 as uuidv4 } from "uuid";

function SimpleChat() {
    const dataChat = localStorage.getItem("chatSession") !== null ? JSON.parse(localStorage.getItem("chatSession")) : [];
    const SECOND_MS = 5000;

    const [userLogin, setUserLogin] = useState({});
    const [userElse, setUserElse] = useState({});
    const [doneRole, setDoneRole] = useState(false)
    const [chatRoom, setChatRoom] = useState(dataChat)
    const [chatRoomFlag, setChatRoomFlag] = useState(false)
    const [message, setMessage] = useState('');

    useEffect(() => {
        Swal.fire({
            icon: 'question',
            title: 'Pilih User Anda!',
            html: '<span>Pilih user yang ingin anda gunakan...</span>',
            showConfirmButton: true,
            showDenyButton: true,
            confirmButtonText: 'Jhon Doe',
            denyButtonText: 'Albert Michel',
        }).then((rslt) => {
            if (rslt.isConfirmed) {
                setUserElse({
                    userId: 2,
                    userName: 'Albert Michel',
                    userImage: 'https://picsum.photos/id/237/60'
                })
                setUserLogin({
                    userId: 1,
                    userName: 'John Doe',
                    userImage: 'https://picsum.photos/id/238/60'
                })
            } else {
                setUserElse({
                    userId: 1,
                    userName: 'John Doe',
                    userImage: 'https://picsum.photos/id/238/60'
                })
                setUserLogin({
                    userId: 2,
                    userName: 'Albert Michel',
                    userImage: 'https://picsum.photos/id/237/60'
                })
            }
            setDoneRole(true)
        })
    }, [])

    useEffect(() => {
        if (chatRoomFlag) {
            localStorage.setItem("chatSession", JSON.stringify(chatRoom))
            setChatRoomFlag(false)
        }
    }, [chatRoom, chatRoomFlag])

    useEffect(() => {
        const interval = setInterval(() => {
            if (localStorage.getItem("chatSession") !== null) {
                const dataStorage = JSON.parse(localStorage.getItem("chatSession"));
                if (dataStorage.length > chatRoom.length) {
                    setChatRoom(dataStorage)
                }
            } else {
                setChatRoom([])
            }
        }, SECOND_MS);

        return () => clearInterval(interval);
    }, [chatRoom.length])

    const handleInputMessage = (data) => {
        setMessage(data)
    }

    const handleSubmit = () => {
        if (message === '') {
            Swal.fire({
                title: "Failed",
                text: "Pesan tidak boleh kosong...",
                icon: "warning"
            })
        } else {
            const newObj = {
                id: uuidv4(),
                userFrom: userLogin.userId,
                userTo: userElse.userId,
                message: message
            }
            setChatRoom((chatRoom) => [...chatRoom, newObj])
            setChatRoomFlag(true)
            setMessage('')
        }
    }

    const handleDeleteChat = () => {
        Swal.fire({
            icon: 'question',
            title: 'Hapus Chat?',
            html: '<span>Chat ini akan dihapus dan tidak bisa dikembalikan...</span>',
            showConfirmButton: true,
            showDenyButton: true,
            confirmButtonText: 'Ya, Hapus',
            denyButtonText: 'Tidak',
        }).then((rslt) => {
            if (rslt.isConfirmed) {
                localStorage.removeItem('chatSession');
                setChatRoom([])
                Swal.fire({
                    title: 'Success',
                    text: 'Chat berhasil dihapus',
                    icon: 'success'
                })
            }
        })
    }

    const truncate = (str) => {
        return str.length > 25 ? str.substring(0, 25) + "..." : str;
    }

    const renderMessage = () => {
        let temp = chatRoom.length > 0 ? chatRoom[chatRoom.length - 1] : null;
        let passMessage = 'Write a message here...'
        if (temp) {
            console.log("MASUK KE IF PERTAMA")
            if (temp.userTo === userLogin.userId) {
                passMessage = temp.message;
            } else {
                passMessage = 'You : ' + temp.message;
            }
        }
        return truncate(passMessage);
    }

    const renderBody = () => {
        if (doneRole) {
            return (
                <Row>
                    {/* LIST CHAT CONTAINER */}
                    <Col xs={6} md={4} lg={4}>
                        <Card className="container-chat">
                            <Card.Body className="listchat">
                                <Form className="searchchat" onSubmit={(e) => e.preventDefault()}>
                                    <Form.Group controlId="search">
                                        <Form.Control type="text" placeholder="Search..." />
                                    </Form.Group>
                                </Form>
                                <ListGroup className="listchat-container">
                                    <ListGroup.Item action variant="light">
                                        <Row>
                                            <Col sm={3} md={3} lg={3}>
                                                <Image src={userElse.userImage} roundedCircle/>
                                            </Col>
                                            <Col sm={9} md={9} lg={9}>
                                                <strong>{userElse.userName}</strong><br />
                                                <span style={{overflow: 'hidden', textOverflow: 'ellipsis'}}>             
                                                    {renderMessage()}
                                                </span>
                                            </Col>
                                        </Row>
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.Body>
                        </Card>
                    </Col>
                    {/* LIST CHAT CONTAINER */}
    
                    {/* BODY CHAT CONTAINER */}
                    <Col xs={6} md={8} lg={8}>
                        <Card className="container-chat">
                            <Card.Header className="bodychat-header">
                                <Row>
                                    <Col sm={1} md={1} lg={1}>
                                        <Image src={userElse.userImage} roundedCircle/>
                                    </Col>
                                    <Col sm={10} md={10} lg={10} className="bodychat-header-info">
                                        <h4>{userElse.userName}</h4>
                                        <span style={{overflow: 'hidden', textOverflow: 'ellipsis'}}>             
                                            <p>Online</p>
                                        </span>
                                    </Col>
                                    <Col sm={1} md={1} lg={1}>
                                        <Button variant="outline-danger" onClick={handleDeleteChat}>X</Button>
                                    </Col>
                                </Row>
                            </Card.Header>
                            <Card.Body className="bodychat">
                                {chatRoom.map((data) => {
                                    if (data.userFrom === userLogin.userId) {
                                        return (
                                            <Card className="chat-myself" key={data.id}>
                                                <Card.Body className="chat-myself-body">{data.message}</Card.Body>
                                            </Card>            
                                        )
                                    } else {
                                        return (
                                            <Card className="chat-somebody" key={data.id}>
                                                <Card.Body>{data.message}</Card.Body>
                                            </Card>            
                                        )
                                    }
                                })}
                            </Card.Body>
                            <Card.Footer >
                                <Form onSubmit={(e) => {
                                    e.preventDefault();
                                    handleSubmit();
                                }}>
                                    <InputGroup className="mb-3">
                                        <FormControl
                                            placeholder="Type something..."
                                            aria-label="bodychat-text"
                                            aria-describedby="basic-addon2"
                                            value={message}
                                            onChange={(e) => handleInputMessage(e.target.value)}
                                        />
                                            <Button variant="outline-secondary" id="button-addon2" onClick={handleSubmit}>
                                                Send
                                            </Button>
                                    </InputGroup>
                                </Form>
                            </Card.Footer>
                        </Card>
                    </Col>
                    {/* BODY CHAT CONTAINER */}
    
                </Row>
            )            
        }
    }

  return (
    <Container className="main">
        {renderBody()}<br/>
    </Container>
  );
}

export default SimpleChat;
