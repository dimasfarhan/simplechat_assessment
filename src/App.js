import { BrowserRouter, Route, Routes } from 'react-router-dom';
import SimpleChat from './pages/SimpleChat';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<SimpleChat />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
